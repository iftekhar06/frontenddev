$(function () {
  $('#fname_error').hide();
  $('#lname_error').hide();
  $('#bdate_error').hide();
  $('#dept_error').hide();
  $('#gender_error').hide();

  var error_fname = false;
  var error_lname = false;
  var error_bdate = false;
  var error_dept = false;
  var error_gender = false;

  $('#fname').focusout(function () {
    check_fname();
  });

  $('#lname').focusout(function () {
    check_lname();
  });

  $('#bdate').focusout(function () {
    check_bdate();
  });

  $('#depertment').focusout(function () {
    check_dept();
  });

  $('#gender').focusout(function () {
    check_gender();
  });

  function check_fname() {
    var pattern = /^[a-zA-Z]*$/;
    var fname = $('#fname').val();
    if (pattern.test(fname) && fname !== '') {
      $('#fname_error').hide();
      $('#fname').css('border-bottom', '2px solid #34F458');
    } else {
      $('#fname_error').html('Enter valid First name');
      $('#fname_error').show();
      $('#fname').css('border-bottom', '2px solid #F90A0A');
      error_fname = true;
    }
  }
  function check_lname() {
    var pattern = /^[a-zA-Z]*$/;
    var lname = $('#lname').val();
    if (pattern.test(lname) && lname !== '') {
      $('#lname_error').hide();
      $('#lname').css('border-bottom', '2px solid #34F458');
    } else {
      $('#lname_error').html('Enter valid Last name');
      $('#lname_error').show();
      $('#lname').css('border-bottom', '2px solid #F90A0A');
      error_lname = true;
    }
  }

  function check_bdate() {
    var bdate = $('#bdate').val();
    if (bdate == null || bdate == '') {
      $('#bdate_error').html('Enter Date Of Birth');
      $('#bdate_error').show();
      $('#bdate').css('border-bottom', '2px solid #F90A0A');
      error_bdate = true;
    } else {
      $('#bdate_error').hide();
      $('#bdate').css('border-bottom', '2px solid #34F458');
    }
  }

  function check_dept() {
    var dept = $('#depertment').val();
    if (dept == null || dept == '') {
      $('#dept_error').html('Enter Depertment');
      $('#dept_error').show();
      $('#depertment').css('border-bottom', '2px solid #F90A0A');
      error_dept = true;
    } else {
      $('#dept_error').hide();
      $('#depertment').css('border-bottom', '2px solid #34F458');
    }
  }

  function check_gender() {
    var gender = $('#gender').val();
    if (gender == null) {
      $('gender_error').html('Select Gender');
      $('#gender_error').show();
      error_gender = true;
    } else {
      $('#gender_error').hide();
    }
  }

  $('#submission_form').submit(function () {
    error_fname = false;
    error_lname = false;
    error_bdate = false;
    error_dept = false;
    error_gender = false;

    check_fname();
    check_lname();
    check_bdate();
    check_dept();
    check_gender();

    if (
      error_fname === false &&
      error_lname === false &&
      error_bdate === false &&
      error_dept === false &&
      error_gender === false
    ) {
      alert('Submission Successfull');
      return true;
    } else {
      alert('Please Fill the form Correctly');
      return false;
    }
  });
});
